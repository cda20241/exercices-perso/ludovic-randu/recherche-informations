import random
from collections import Counter

list=[['pascal', 'rose', 43 ],
      ['mickaël', 'fleur', 29],
      ['henry', 'tulipe', 35],
      ['michel', 'framboise', 35],
      ['arthur', 'petale', 35],
      ['michel', 'pollen', 50],
      ['michel', 'framboise', 42]]

random.shuffle(list)
def recherche(prenom, age):
      """"
      Fonction qui prends en paramètres le prénom et l'âge recherchés et qui fait une liste de ces personnes
      """
      liste_personne = []
      for personne in list:
           if personne[0] == prenom or personne[2] >= age:
                 liste_personne.append(personne)
      return liste_personne

def search(nom, index):
      liste_personne = []
      for personne in list:
            if personne[index]==nom :
                  liste_personne.append(personne)
      return liste_personne
def nbr_lettre():
      """
      Fonction qui retourne la liste de personne qui ont le meme nombre de lettres dans leur nom et prénom
      :return:
      """
      liste_personne = []
      for personne in list:
            if len(personne[0]) == len(personne[1]):
                  liste_personne.append(personne)
      return liste_personne

def doublons(index):
      list_personne = []
      for personne in list:
            list_personne.append(personne[index])
      list_p = Counter(list_personne)
      for prenom, nbr in list_p.items():
            if nbr > 1:
                  liste = search(prenom, index)
                  liste_m = []
                  for a in liste:
                        liste_m.append(a)
                  return liste_m

# 1- Etape 1: affiche un message si une personne s'appelle Michel ou est agé de plus de 50ans
liste_personne = recherche('michel',50)
if len(liste_personne) >=1 :
      print("On a dans la liste quelqu'un qui adore le camping !")
#2 Etape 2: Affiche toutes les infos des personnes de l'étape 1
for personne in liste_personne:
      print("C'est formidable "+personne[0] + ' ' + personne[1] + ', ' + str(personne[2]) + ' ans adore le camping' )
#3 on recherche les personnes qui ont le meme nombre de lettres dans leur prenom que dans leur nom
liste_personne = nbr_lettre()
for personne in liste_personne:
      print("C'est formidable "+personne[0] + " " + personne[1] + " a "+ str(len(personne[0])) +" lettres dans son nom et son prénom !")
#4
prenom=doublons(0)
phrase=""
for personne in prenom:
      phrase += personne[0] + " " + personne[1] + ", "+ str(personne[2])+ " ans et "
print(phrase+" ... ont des prénoms identiques")
#5
nom=doublons(1)
phrase=""
for personne in nom:
      phrase += personne[0] + " " + personne[1] + ", "+ str(personne[2])+ " ans et "
print(phrase+" ... ont des noms identiques")
#6
age=doublons(2)
phrase=""
for personne in age:
      phrase += personne[0] + " " + personne[1] + ", "+ str(personne[2])+ " ans et "
print(phrase+" ... ont le même âge")


